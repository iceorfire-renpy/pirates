﻿define p = Character("Player")
define s = Character("Squid")
define f = Character("Miss Fluffybottom")

label start:
    scene bg insidehouse
    show pirate happy

    p "No Miss Fluffybottom! Wait!"
    "Your cat, the adventurous (and naughty) Miss Fluffybottom runs out the front door of your house. That bad kitty's chasing a bird again! You should probably go after her right meow."

    menu:
        "Go after her.":
            jump house_outside
        "She'll be fine.":
            jump house_inside

    return

label house_inside:
    scene bg insidehouse
    show pirate happy
    p "Just your normal run-of-the-mill house. It's not much but it's home. Mom doesn't seem to be home right now."
    p "Maybe I should go check on Miss Fluffybottom."

    menu:
        "Check on Ms Fluffybottom.":
            jump house_outside

label house_outside:
    scene bg outsidehouse
    show pirate happy

    p "There she is down on the beach chasing the seagulls again. Crazy cat! I should probably bring her back in the house before she gets lost or drowns."
    "The door slams shut and you're locked out of the house!"

    menu:
        "Knock on the door.":
            jump door_knock
        "Go after Ms Fluffybottom.":
            jump beach_squid

label door_knock:
    scene bg outsidehouse
    show pirate happy

    "Knock, knock, knock!"
    p "I guess Mom's not home."

    return

label beach_squid:
    scene bg beach
    show squid

    "As you reach for your cat, a sea creature rises out of the sea."
    s "Boo! Sorry... I mean Roar!"
    "The squid, who definitely doesn't speak English, roars and grabs poor Miss Fluffybottom! Today would've been a good day for a catnap but too late for that."

    show pirate happy at right

    p "Let my cat go you under-cooked calamari!"
    f "Meow!!!"
    "The squid, holding your terrified feline, wipes tears from her eyes. (That calamari insult hurt.) She dives under the waves."

    hide squid

    p "Wow! Mom's gonna be mad."

    show squid

    "The giant squid pops her head above the water and wiggles her tentacles in disgust."
    s "Roar! Gurgle. (Yuck! I guess cousin Johnny is right... cats really are nasty.)"
    "The squid spits out the water-logged Miss Fluffybottom who flies through the air and lands on..."
    "a docked pirate ship!"

    p "Ok... correction. Mom's gonna be SUPER mad. Somehow I have to get on that pirate ship and rescue the cat."
    p "I can start at the pier or those palm trees further down the beach."
