style inv_item is text:
    size 16
    bold True
    color Color((0, 255, 0, 255))
    pos (5,125)

style inventory_frame is frame:
    xpadding 10
    ypadding 10
    xalign 0.5
    yalign 0.5
    xsize 1152
    ysize 660
    background Color((193, 66, 66, 192))

style close_btn:
    xpos 1100
    ypos -4

style hud_frame is frame:
    xpadding 10
    ypadding 10
    xalign 0.5
    yalign 0.0
