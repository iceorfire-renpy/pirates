screen frontPorchScreen():
    imagebutton:
        xalign 0.75
        yalign 1.0
        idle "flower"
        action Jump("flower_pot")

screen hudScreen():
    frame style style["hud_frame"]:
        hbox:
            if player_name == "":
                label "Player"
            else:
                label player_name

            label "   Points: "
            text "%d  " % points

            imagebutton:
                idle "backpack"
                action Show("inventoryScreen")

screen inventoryScreen():
    modal True
    frame style style["inventory_frame"]:
        imagebutton style style["close_btn"]:
            idle "close"
            action Hide("inventoryScreen")

        vbox:
            text "Inventory"
            grid 7 4:
                for i in range(28):
                    frame:
                        maximum(155,155)
                        if i < len(inventory):
                            background Image(inventory[i]+".png")
                            $ inv_item_name = inventory[i].replace('_', ' ')
                            text [inv_item_name] style style["inv_item"]
                        else:
                            background Image("placeholder.png")
